/* create a new file at specified address and write name in it */ 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
 
  char address[40]="";

  // create a file pointer
  FILE *new_file;

  strcpy(address,argv[1]);

  new_file=fopen(address,"w+");

  // write name in new_file
  fputs("yuechenchen\n",new_file);

  fclose(new_file);

  return 0; 

} 
